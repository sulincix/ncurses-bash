printw(){
echo -ne "$*"
}
initscr(){
trap 'getmaxyx' WINCH
printw "\033[s\033c"
}

endscr(){
printw "\033c\033[u"
}
getch(){
read -s -n 1 c
ord $c
}

move(){
printw "\033[$2;$1f"
}
mvprintw(){
move $1 $2
printw $3
}

getyx(){
printw "\033[6n"
read -sdR CURPOS
CURPOS=${CURPOS#*[}
export CURX=$(echo $CURPOS | sed "s/;.*$//g")
export CURY=$(echo $CURPOS | sed "s/^.*;//g")
}

noecho(){
printw "\033[?25l"
}

yesecho(){
printw "\033[?250"
}
chr() {
  printf \\$(printf '%03o' $1)
}

ord() {
  printf '%d' "'$1"
}

drawbox(){
x1=$2
x2=$4
y1=$1
y2=$3
mvprintw $x1 $y1 "╔"
mvprintw $x1 $y2 "╚"
mvprintw $x2 $y1 "╗"
mvprintw $x2 $y2 "╝"
for i in $(seq $(($x1+1)) 1 $(($x2-1))) ; do
  mvprintw $i $y1 "═"
  mvprintw $i $y2 "═"
done
for i in $(seq $(($y1+1)) 1 $(($y2-1))) ; do
  mvprintw $x1 $i "║"
  mvprintw $x2 $i "║"
done
}

attron(){
for i in $*
do
	case $i in
		A_NORMAL)
		printw "\033[;0m"
		;;
		A_UNDERLINE)
		printw "\033[4m"
		;;
		A_REVERSE)
		printw "\033[7m"
		;;
		A_BLINK)
		printw "\033[5m"
		;;
		A_DIM)
		printw "\033[2m"
		;;
		A_BOLD)
		printw "\033[1m"
		;;
		A_INVIS)
		printw "\033[8m"
		;;
		C_BLACK)
		printw "\033[30m"
		;;
		C_RED)
		printw "\033[31m"
		;;
		C_GREEN)
		printw "\033[32m"
		;;
		C_YELLOW)
		printw "\033[33m"
		;;
		C_BLUE)
		printw "\033[34m"
		;;
		C_MAGENTA)
		printw "\033[35m"
		;;
		C_CYAN)
		printw "\033[36m"
		;;
		C_WHITE)
		printw "\033[37m"
		;;
		B_BLACK)
		printw "\033[40m"
		;;
		B_RED)
		printw "\033[41m"
		;;
		B_GREEN)
		printw "\033[42m"
		;;
		B_YELLOW)
		printw "\033[43m"
		;;
		B_BLUE)
		printw "\033[44m"
		;;
		B_MAGENTA)
		printw "\033[45m"
		;;
		B_CYAN)
		printw "\033[46m"
		;;
		B_WHITE)
		printw "\033[47m"
		;;
	esac
done
}
addch(){
printw $(chr $1)
}

