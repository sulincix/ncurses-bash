. ncurses.sh

initscr
noecho
move 40 40
printw "Hello"
move 22 3
getyx
attron B_MAGENTA
drawbox 5 3 20 30
move 0 0
attron C_RED
printw "$CURX-$CURY\n"
attron B_BLUE
printw "c for exit and a for write and b for delete a"
attron A_NORMAL C_GREEN B_BLUE
while true
do
  c=$(getch)
  if [ $c -eq 97 ] ; then
    mvprintw 10 8 "$(chr $c)"
  elif [ $c -eq 99 ]; then
    break
  elif [ $c -eq 98 ]; then
    move 10 8 ; echo -n " "
  else
    mvprintw 10 10 "$c"
  fi
done
yesecho
endscr
